# typonazi

Typonazi est une extension Chrome qui apporte un menu contextuel pour ajouter des caractères typographiques utiles (en français) lors de l'édition de texte dans le navigateur.


## Screenshot

![Menu contextuel d'ajout de charactère](https://bitbucket.org/ealprr/typonazi/wiki/menu-contextuel-01.png)


## Installation

Télécharger simplement le fichier crx depuis chrome/chromium : [typonazi-02.crx](https://bitbucket.org/ealprr/typonazi/downloads/typonazi-02.crx)


## Licence

GPL3

